import { MonAnV2 } from "../../models/v2/monAnModel-v2.js";

export function layThongTinTuForm() {
  let ma = document.getElementById("foodID").value;
  let ten = document.getElementById("tenMon").value;
  let loai = document.getElementById("loai").value;
  let gia = document.getElementById("giaMon").value;
  let khuyenMai = document.getElementById("khuyenMai").value;
  let tinhTrang = document.getElementById("tinhTrang").value;
  let hinhMon = document.getElementById("hinhMon").value;
  let moTa = document.getElementById("moTa").value;

  let monAn = new MonAnV2(
    ma,
    ten,
    loai,
    gia,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa
  );
  return monAn;
}

export function renderDanhSachMonAn(foodList) {
  let contentHTML = "";
  foodList.forEach((item) => {
    let contentTr = /*html*/ `
    <tr>
    <td>${item.ma}</td>
    <td>${item.ten}</td>
    <td>${item.loai}</td>
    <td>${item.gia}</td>
    <td>${item.phanTramKm * 100}%</td>
    <td>${item.tinhGiaKM()}</td>
    <td>${item.tinhTrang}</td>
    </tr>
    `;
    contentHTML += contentTr;
  });
  document.getElementById("tbodyFood").innerHTML = contentHTML;
}
