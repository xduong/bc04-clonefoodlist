export class MonAnV2 {
  constructor(ma, ten, loai, gia, phanTramKm, tinhTrang, hinhMon, moTa) {
    this.ma = ma;
    this.ten = ten;
    this.gia = gia;
    this.phanTramKm = phanTramKm;
    this.tinhTrang = tinhTrang;
    this.hinhMon = hinhMon;
    this.moTa = moTa;
    this.loai = loai;
  }
  tinhGiaKM() {
    return this.gia * (1 - this.phanTramKm);
  }
}
